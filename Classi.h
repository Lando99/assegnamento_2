//
//  Classi.h
//  Sistema di approvvigionamento
//
//  Created by Matteo Lando on 25/01/2020.
//  Copyright © 2020 Matteo Lando. All rights reserved.
//

#ifndef Classi_h
#define Classi_h
#include<iostream>
#include <vector>
using namespace std;

class Componente{
    int id;
    string nome;
    int tempoSpedizione;
    float p1,p2,p3;
public:
    Componente(){
        id = 0;
        nome = "";
        tempoSpedizione = 0;
        p1 = 0.0;
        p2 = 0.0;
        p3 = 0.0;
    }
    Componente (const Componente& C){
        this->id = C.id;
        this->nome = C.nome;
        tempoSpedizione = C.tempoSpedizione;
        p1 = C.getP1();
        p2 = C.getP2();
        p3 = C.getP3();
    }
    
    void setId(int id){ this->id = id; }
    void setNome(string nome){  this->nome= nome; }
    int getId()const{   return this->id; }
    string getNome()const{  return nome; }
    void setTempoSpedizione(int tempo){ tempoSpedizione = tempo; }
    int getTempoSpedizione()const{   return tempoSpedizione; }
    
    void setP1(float prezzo){   p1 = prezzo; }
    void setP2(float prezzo){   p2 = prezzo; }
    void setP3(float prezzo){   p3 = prezzo; }
    float getP1()const{ return p1; }
    float getP2()const{ return p2; }
    float getP3()const{ return p3; }
    
    
    
    // uso di overloading di = per componente
    void operator = (const Componente &C){
        this->id = C.id;
        this->nome = C.nome;
        tempoSpedizione = C.tempoSpedizione;
        p1 = C.p1;
        p2 = C.p2;
        p3 = C.p3;
    }
    
    
};

// struct template che associa un tipo di oggetto come può essere un componete o un modello alla qta.
// Es. accociare il modello alla quantità ordinata
template <typename T>
struct Oggetto_Qta{
    T oggetto;
    int qta;
    
    inline Oggetto_Qta operator=(Oggetto_Qta a) {
        oggetto = a.oggetto;
        qta = a.qta;
        return a;
    }
    /*
    ~Oggetto_Qta(){
        delete oggetto;
    }
     */
    
    
};

class Modello{
    int id;
    string nome;
    vector<Oggetto_Qta<Componente>> vettore_componenti;
    float prezzo;
public:
    Modello(){
        id =0;
        nome = "";
        prezzo=0.0;
    }
    Modello(const Modello &M){
        this->id = M.id;
        this->nome = M.nome;
        this->vettore_componenti = M.vettore_componenti;
        this->prezzo = M.prezzo;
    }
    
    void setId(int id){
        this->id = id;
    }
    void setNome(string nome){
        this->nome = nome;
    }
    int getId() const{
        return id;
    }
    string getNome() const{
        return this->nome;
    };
    void setListComponent(vector<Oggetto_Qta<Componente>> vettore){
        
        vettore_componenti = vettore;
    }
    vector<Oggetto_Qta<Componente>> getLista() const{
        return vettore_componenti;
    }
    void setPrezzo(float prezzo){
        this->prezzo = prezzo;
    }
    float getPrezzo()const{
        return prezzo;
    }
    
    // uso di overloading di = per modello
    void operator = (const Modello &M){
        this->id = M.id;
        this->nome = M.nome;
        this->vettore_componenti = M.vettore_componenti;
        this->prezzo = M.prezzo;
    }
};


/* variabile stato indica lo stato dell'ordine
a = attesa
p = prduzione
c = completo;
*/
class OrdineModello{
    Modello modello; // modello che voglio produrre
    int mese; // mese di arrivo comanda
    int qta;
    string stato = "a";
public:
    OrdineModello(){
        mese = 0;
        qta = 0;
    }
    OrdineModello(const OrdineModello &O){
        this->mese = O.mese;
        this->modello = O.modello;
        this->qta = O.qta;
    }
    
    void setMese(int mese){
        this->mese = mese;
    }
    void setModello(Modello m){
        this->modello = m;
    }
    void setQta(int qta){
        this->qta = qta;
    }
    int getMese() const{
        return mese;
    }
    Modello getModello() const{
        return modello;
    }
    int getQta() const{
        return qta;
    }
    void setStatoProduzione(){
        stato = "p";
    }
    void setStatoCompletato(){
        stato = "c";
        // funzione che incrementa l'incasso dell'azienda
    }
    string getStato()const{
        return stato;
    }
    
    // uso di overloading di = per OrdineModello
    void operator =(const OrdineModello &O){
        this->mese = O.mese;
        //delete this->modello;
        this->modello = O.modello;
        this->qta = O.qta;
    
    }
};


class OrdineComponenti{
    int mese; // mese in cui viene ordinato il pezzo
    int qta; // quantità richiesta
    Componente componente; // componente da ordinare
    
public:
    
    void setMese(int mese){
        this->mese = mese;
    }
    void setComponente(Componente componente){
        this->componente = componente;
    }
    void setQta(int qta){
        this->qta = qta;
    }
    
    int getMese()const{
        return mese;
    }
    Componente getComponente()const{    return componente; }
    int getQta()const{  return qta; }
    
    
};



ostream& operator<< (ostream &out, const Componente &componente);
ostream& operator<< (ostream &out, const Modello &modello);
ostream& operator<< (ostream &out, const OrdineModello &ordineModello);
ostream& operator<< (ostream &out, const OrdineComponenti &ordineComponente);

#endif /* Classi_h */
