//
//  readerFile.h
//  Sistema di approvvigionamento
//
//  Created by Matteo Lando on 20/01/2020.
//  Copyright © 2020 Matteo Lando. All rights reserved.
//



#ifndef readerFile_h
#define readerFile_h
#include <vector>
#include "Classi.h"
using namespace std;





vector<string> letturaModelli();
vector<Componente> letturaInfoComponenti();
Modello letturaModello(const string nameFile, vector<Componente> recordComponenti  );


vector<OrdineModello> letturaOrdini( vector<Modello>& recordModelli, float& cassa);
#endif /* readerFile_h */
