//
//  Azienda.h
//  Sistema di approvvigionamento
//
//  Created by Matteo Lando on 26/01/2020.
//  Copyright © 2020 Matteo Lando. All rights reserved.
//

#ifndef Azienda_h
#define Azienda_h

class Azienda{
    float cassa;
    int mese = 0;
    vector<Modello> modelli;
    vector<Componente> componenti;
    vector<OrdineModello> ordineModelli;
    
    vector<OrdineComponenti> pezziOrdinati;
    vector<OrdineComponenti> magazzino;
    
public:
    void lettura_file();
   
    void nuovo_mese();
    
    
    void ordiniNuovoMese();
    bool checkOrdine(const OrdineComponenti& ordine);
    bool isTuttiOrdiniEvasi();
    void stoccaggioMagazzino();
    void avvioProduzione();
    bool isInMagazzino(Oggetto_Qta<Componente> componente_qta);
    void termineProduzione();
    
    // Funzioni di stampa :
    void stampa();
    void stampaMagazzino();
    void stampaAcquistiEffettuati();
    void stampaOrdiniEvasi();
    void stampaModelli();
    void stampaOrdini();
    void stampaComponenti();
    
    float getCassa()const{
        return cassa;
    }
    
    

};

#endif /* Azienda_h */
