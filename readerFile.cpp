//
//  readerFile.h
//  Sistema di approvvigionamento
//
//  Created by Matteo Lando on 20/01/2020.
//  Copyright © 2020 Matteo Lando. All rights reserved.
//

#include <iostream>
using namespace std;
#include <fstream>
#include <vector>
#include "readerFile.h"
#include <algorithm>
/*  TODO:
eliminare codice indicato come "riga da togliere"
 
 */

// riga da togliere

const string modelsPath = "models.dat";
const string infoPath = "components_info.dat";
const string orderPath = "orders.dat";

// MODIFICATO ADESSO
vector<string> letturaModelli(){
    vector<string> names;
    
    ifstream OpenFile(modelsPath);
    
    if(OpenFile.is_open()==false){
        cout<<"File non trovato";
    }else{
        string parola;
        
        while(!OpenFile.eof())
        {
            getline(OpenFile, parola);
            names.push_back(parola);
        }
        
        OpenFile.close();
    }
    return names;
}
// MODIFICATO ADESSO
vector<Componente> letturaInfoComponenti(){
    vector<Componente>  componenti;
    ifstream OpenFile(infoPath);
    
    if(OpenFile.is_open()==false){
        cout<<"File non trovato";
    }else{
        string parola;
        Componente aux;
       
        while(!OpenFile.eof())
        {
            //leggo id
            getline(OpenFile, parola, ' ');
            aux.setId(stoi(parola));
            
            //leggo nome
            getline(OpenFile, parola, ' ');
            aux.setNome(parola);
           
            
            //leggo tempoSpediione
            getline(OpenFile, parola, ' ');
            aux.setTempoSpedizione(stoi(parola));
            
    
            //leggo prezzo 1
            getline(OpenFile, parola, ' ');
            aux.setP1(stof(parola));
            
            //leggo prezzo 2
            getline(OpenFile, parola, ' ');
            aux.setP2(stof(parola));
            
            //leggo prezzo 3
            getline(OpenFile, parola);
            aux.setP3(stof(parola));
            
            componenti.push_back(aux);
        }
        
        OpenFile.close();
    }
    return componenti;
    
    
}
// MODIFICATO ADESSO
Modello letturaModello(const string nameFile, vector<Componente> recordComponenti ){
    vector<Oggetto_Qta<Componente>> auxComponenti;
    string nome;
    Modello auxModello;
    
    string fileName = nameFile;
    
    ifstream OpenFile(fileName);
       
       if(OpenFile.is_open()==false){
           cout<<"File non trovato";
       }else{
           string parola;
           
           //lettura della prima riga contente info del modello
           if(!OpenFile.eof()){
               
               getline(OpenFile, parola, ' ');
               auxModello.setId(stoi(parola));
               
               getline(OpenFile, parola, ' ');
               auxModello.setNome(parola);
               
               getline(OpenFile, parola);
               auxModello.setPrezzo(stof(parola));
        
           }
           //lettura del resto del file
           while(!OpenFile.eof())
           {
               
               getline(OpenFile, parola, ' ');
               
               
               bool trovato = false;
               int i = 0;
               Oggetto_Qta<Componente> auxComponente;
               while (trovato == false) {
                   if(recordComponenti[i].getId()==stoi(parola)){
                       //quando viene trovato
                       trovato = true;
                       auxComponente.oggetto = recordComponenti[i];
                      
                       getline(OpenFile, parola, ' ');
                       getline(OpenFile, parola);
                       auxComponente.qta = stoi(parola);
                       auxComponenti.push_back(auxComponente);
                   }

                   
                   i++;
               }
               
           }
          
           
           auxModello.setListComponent(auxComponenti);
           
           
           OpenFile.close();
       }
   
       return auxModello;
    
}
// MODIFICATO ADESSO
vector<OrdineModello> letturaOrdini( vector<Modello>& recordModelli, float &cassa){
    string parola;
    vector<OrdineModello> ordini;
    
    OrdineModello auxOrdine;
    
    ifstream OpenFile(orderPath);
    
    
    if(OpenFile.is_open()==false){
        cout<<"File non trovato";
    }else{
        
        getline(OpenFile, parola);
        cassa = stoi(parola);
        
        while(!OpenFile.eof())
        {
            getline(OpenFile, parola, ' ');
            auxOrdine.setMese(stoi(parola));
            
            getline(OpenFile, parola, ' ');
           
            bool trovato = false;
            int i = 0;
            
            while (trovato == false) {
                
                if(recordModelli[i].getId()==stoi(parola)){
                    //quando viene trovato
                    trovato = true;
                    auxOrdine.setModello(recordModelli[i]);
                    
                }
                i++;
            }
            
            getline(OpenFile, parola);
            auxOrdine.setQta(stoi(parola));
            
            ordini.push_back(auxOrdine);
        }
        
        OpenFile.close();
       
    }
     return ordini;
}

