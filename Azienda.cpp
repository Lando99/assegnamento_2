//
//  Azienda.cpp
//  Sistema di approvvigionamento
//
//  Created by Matteo Lando on 23/01/2020.
//  Copyright © 2020 Matteo Lando. All rights reserved.
//


#include <iostream>
#include <vector>
#include "readerFile.h"
#include "Azienda.h"

using namespace std;

void Azienda::lettura_file(){
    componenti = letturaInfoComponenti();
    
    vector<string> files = letturaModelli();
    for (int i = 0; i<files.size(); i++) {
       modelli.push_back(letturaModello(files[i], componenti));
    }
    
    ordineModelli = letturaOrdini(modelli, cassa);
        
}
void Azienda::stampaOrdini(){
    cout<<"* ORDINI ARRIVATI *";
    for (auto &orderine_modello : ordineModelli) // access by reference to avoid copying
    {
        cout<<orderine_modello;
    }
}
void Azienda::stampaModelli(){

    
    for (auto modello : modelli) // access by reference to avoid copying
    {
        cout<<modello;
    }
    
}
void Azienda::stampaComponenti(){
    for (auto &componente : componenti) // access by reference to avoid copying
    {
        cout<<componente;
    }
}

void Azienda::ordiniNuovoMese(){
    //prendo gli ordini del mese corrente
    vector<OrdineComponenti> pezziDaOrdinare;
    OrdineComponenti auxOrdine;
    
    for(auto &ordine : ordineModelli){
        if(ordine.getMese() == mese){
            for(auto &componente : ordine.getModello().getLista()){
                bool trovato = false;
                // se è presente già quel componente nel vettore dei pezzi da ordinare allora incremento la quantità
                for(auto &auxComp : pezziDaOrdinare){
                    if(auxComp.getComponente().getId() == componente.oggetto.getId()){
                        auxComp.setQta(auxComp.getQta()+componente.qta);
                        trovato = true;
                    }
                }
                //se non ho trovato il componente lo inserisco negli ordini da fare
                if(trovato == false){
                    auxOrdine.setComponente(componente.oggetto);
                    auxOrdine.setQta(componente.qta);
                    auxOrdine.setMese(mese);
                    
                    if(checkOrdine(auxOrdine)){
                        pezziOrdinati.push_back(auxOrdine);
                    }else{
                        cout<<"AVVISO: Non ho abbastanza soldi per effettuare l'ordine";
                    }
                }
                
            }
        }
    }
    
}
// funzione che controlla la liquindità della cassa per comprare i componenti
bool Azienda::checkOrdine(const OrdineComponenti& ordine){
    float costoOrdine = 0.0;
    if(ordine.getQta() > 0 && ordine.getQta()<= 10){
        costoOrdine += ordine.getQta()*ordine.getComponente().getP1();
    }else if(ordine.getQta() > 10 && ordine.getQta()<= 50){
        costoOrdine += ordine.getQta()*ordine.getComponente().getP2();
    }else{
        costoOrdine += ordine.getQta()*ordine.getComponente().getP3();
    }
    
    if(costoOrdine <= cassa){
        cassa -= costoOrdine;
        return true;
        
    }
    return false;
}


void Azienda::stoccaggioMagazzino(){
    for(auto &pezzi : pezziOrdinati){
        if(mese - pezzi.getMese() == pezzi.getComponente().getTempoSpedizione()){
            magazzino.push_back(pezzi);
        }
    }
}

void Azienda::avvioProduzione(){
    bool messaInProduzione = true;
    bool eliminato = false;
    
    int i = -1;
    for(auto &ordine : ordineModelli){
        i++;
        if(ordine.getStato() == "a"){
            
            for(auto &pezzo : ordine.getModello().getLista()){
                
                if(!isInMagazzino(pezzo)){
                    
                    messaInProduzione = false;
                }
            }
            if(messaInProduzione){
                cout<<"VADO IN PRODUZIONE";
                // prendo i pezzi dal magazzino
                for(auto &pezzo : ordine.getModello().getLista()){
                    for(auto &pezzoMagazzino : magazzino){
                        if(pezzo.oggetto.getId() == pezzoMagazzino.getComponente().getId()&& !eliminato){
                            pezzoMagazzino.setQta(pezzoMagazzino.getQta()-pezzo.qta);
                            eliminato = true;
                        }
                    }
                }
                ordineModelli[i].setStatoProduzione();
            }
            
        }
       
    }
    
}
//cerca se un pezzo è in magazzino
bool Azienda::isInMagazzino(Oggetto_Qta<Componente> componente_qta){
    bool trovato = false;
    
    for(auto &pezzoMagazzino : magazzino){
        if(pezzoMagazzino.getComponente().getId() == componente_qta.oggetto.getId()){
            if(pezzoMagazzino.getQta() >= componente_qta.qta){
                trovato = true;
            }
        }
    }
    return trovato;
}


void Azienda::termineProduzione(){
    bool stampa = false;
    for(auto &ordine : ordineModelli){
        
        if(ordine.getStato() == "p"){
            cout<<"\n\nIN PRODUZIONE\n";
            ordine.setStatoCompletato();
            cassa += ordine.getModello().getPrezzo() * ordine.getQta();
            stampa = true;
        }
    }
    if(stampa)
        // Richiamo la funzione stampa se nel mese corrente sono stati evasi degli ordini
        this->stampa();
}
// restituisce true se non ho evaso tutti gli ordini che mi sono stati fatti
// Appena ho completato tutti gli ordini aresto il programma
void Azienda::nuovo_mese(){
    
    mese ++;
    
    termineProduzione();
    stoccaggioMagazzino();
    
    ordiniNuovoMese();
    avvioProduzione();
    
    
}

bool Azienda::isTuttiOrdiniEvasi(){
    bool finito = true;
    for(auto &ordine : ordineModelli){
        if(ordine.getStato() != "c"){
            finito = false;
        }
    }
    return finito;
}

void Azienda::stampa(){
    this->stampaAcquistiEffettuati();
    this->stampaMagazzino();
    this->stampaOrdiniEvasi();
}
void Azienda::stampaOrdiniEvasi(){
    cout<<"\nORDINI COMPLETATI";
    for(auto &ordine : ordineModelli){
        if(ordine.getStato() == "c"){
            cout<<ordine;
        }
    }
}
void Azienda::stampaMagazzino(){
    cout<<"\nSTAMPA MAGAZZINO :";
    for(auto &pezzi : magazzino ){
        cout<<pezzi;
    }
    if(magazzino.size() == 0){
        cout<<"Magazzino VUOTO";
    }
}
void Azienda::stampaAcquistiEffettuati(){
    cout<<"\nSTAMPA  COMPONENTI ORDINATI :";
    for(auto &pezzi : pezziOrdinati ){
        cout<<pezzi;
    }
    if(pezziOrdinati.size() == 0){
        cout<<"Magazzino VUOTO";
    }
    
}

