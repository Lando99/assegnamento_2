//
//  Classi.cpp
//  Sistema di approvvigionamento
//
//  Created by Matteo Lando on 25/01/2020.
//  Copyright © 2020 Matteo Lando. All rights reserved.
//

#include <stdio.h>
#include "Classi.h"

ostream& operator<< (ostream &out, const Componente &componente){
    out<<"\n\t"<<"id_comp :"<<componente.getId()<<" nome_comp :"<<componente.getNome();
    out<<"\t temp.sped :"<<componente.getTempoSpedizione()<<"  p1: "<<componente.getP1()<<"   p2: "<<componente.getP2()<<"  p3: "<<componente.getP3();
    return out;
}

ostream& operator<< (ostream &out, const Modello &modello)
{
    
    out << "\nid_modello :"<<modello.getId()<<" nome_modello:"<<modello.getNome()<<" prezzo :"<<modello.getPrezzo()<<"\n";
    out<<"lista componenti :";
    for (auto &componente : modello.getLista()) // access by reference to avoid copying
        {
            out<<componente.oggetto<<" qta :"<<componente.qta;
        }
    
    return out;
}

ostream& operator<< (ostream &out, const OrdineModello &ordineModello)
{
    out << "\nMese :"<<ordineModello.getMese()<<" nome.model :"<<ordineModello.getModello().getNome()<<" qta:"<<ordineModello.getQta()<<" stato : "<<ordineModello.getStato();
    return out;
}


ostream& operator<< (ostream &out, const OrdineComponenti &ordineComponente)
{
   
    out << "\nMese :"<<ordineComponente.getMese()<<" nom.comp :"<<ordineComponente.getComponente().getNome()<<" qta: "<<ordineComponente.getQta();
    return out;
}

